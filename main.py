import machine
import network
import time
import ubinascii
import webrepl
import dht
import ujson as json
import webrepl
from lib.umqtt import MQTTClient

# These defaults are overwritten with the contents of /config.json by load_config()
CONFIG = {
    "wifi_ssid": "ssid",
    "wifi_password": "password",
    "broker": "10.0.0.1",
    "client_id": b"esp8266_" + ubinascii.hexlify(machine.unique_id()),
    "topic_temp": b"temperature/roomname",
    "topic_humid": b"humidity/roomname",
    "topic_conf": b"config/roomname"
}

client = None
ifconf = None
d = None

def do_connect():
    global ifconf
    SSID = CONFIG['wifi_ssid']
    PASSWORD = CONFIG['wifi_password']
    sta_if = network.WLAN(network.STA_IF)
    ap_if = network.WLAN(network.AP_IF)

    if ap_if.active():
        ap_if.active(False)

    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(SSID, PASSWORD)
        while not sta_if.isconnected():
            pass

    ifconf = sta_if.ifconfig()
    print('Network configuration:', ifconf)

def setup_pins():
    global d
    global sleep_switch
    d = dht.DHT11(machine.Pin(0))
    sleep_switch = machine.Pin(5, machine.Pin.IN)

def load_config():
    try:
        with open("/config.json") as f:
            config = json.loads(f.read())
            print("Config file /config.json found")
    except (OSError, ValueError):
        print("Couldn't load /config.json")
        save_config()
    else:
        CONFIG.update(config)
        print("Loaded config from /config.json")

def save_config():
    try:
        with open("/config.json", "w") as f:
            f.write(json.dumps(CONFIG))
    except OSError:
        print("Couldn't save /config.json")

def main():
    client = MQTTClient(CONFIG['client_id'], CONFIG['broker'])
    client.connect()
    print("Connected to {}".format(CONFIG['broker']))
    while True:
        d.measure()
        data_temp = d.temperature()
        data_humid = d.humidity()
        client.publish('{}/{}'.format(CONFIG['topic_conf'], CONFIG['client_id']), bytes(str(ifconf), 'utf-8'))
        client.publish('{}/{}'.format(CONFIG['topic_temp'], CONFIG['client_id']), bytes(str(data_temp), 'utf-8'))
        client.publish('{}/{}'.format(CONFIG['topic_humid'], CONFIG['client_id']), bytes(str(data_humid), 'utf-8'))
        print('Temperature: {}'.format(data_temp))
        print('Humidity: {}'.format(data_humid))
        print('Sleep switch status: {}'.format(sleep_switch.value()))
        if sleep_switch.value() == 0:
            print('Going to deepsleep in 5 seconds')
            time.sleep(5)
            rtc = machine.RTC()
            rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)
            rtc.alarm(rtc.ALARM0, 20*1000*60)
            print('Going to deepsleep now for 20 minutes')
            machine.deepsleep()
        else:
            print('Sleeping for 20 minutes')
            time.sleep(20*60)

if __name__ == '__main__':
    load_config()
    setup_pins()
    do_connect()
    try:
        webrepl.start()
    except:
        print("WebREPL configuration not done")
    main()
