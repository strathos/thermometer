# MQTT Thermometer with ESP8266 running MicroPython

### Components used
- Wemos D1 Mini Pro
- DHT11 (connected to pin D3 - GPIO0)
- 2 position micro switch (connected to D1 - GPIO5)

### Basics functionality
The ESP8266 board being used is Wemos D1 Mini Pro and the sensor for temperature and humidity is DHT11. DHT11 is connected to pin D3 which is GPIO0. Values are being read every 20 minutes and reported to Home Assistant via MQTT broker. By default the board will deep sleep the 20 minutes periods, but if micro switch connected to D1 is set to 3,3 V (true) the board sleeps normally for 20 minutes. The idea behind the switch is to be able to reflash the board easier.
For the deep sleep to work, D0 (GPIO16) needs to be connected to RST.
Configuration is stored in config.json file which is created by main.py if config.json does not already exist in the file system root.

### MicroPython build instructions for Ubuntu 14.04

#### Install packages:

    sudo apt install python-pip gperf build-essential git autoconf automake libtool g++ flex bison texinfo gawk ncurses-dev libexpat-dev libreadline-dev libffi-dev help2man python-dev picocom
    sudo pip install esptool
    sudo pip install adafruit-ampy

#### Test connection to esp8266
    sudo esptool.py --port /dev/ttyUSB0 chip_id
    sudo esptool.py --port /dev/ttyUSB0 read_mac
    sudo picocom -b 115200 -r -l /dev/ttyUSB0
    (To exit picocom, use ctrl+a followed by ctrl+x)

#### Create a project directory under your home directory and enter it
    mkdir -p ~/Projects/esp8266 && cd $_
    # OR IF PYTHON3 IS DEFAULT
    cd ~
    virtualenv -p /usr/bin/python2 --distribute esp8266
    cd esp8266
    source bin/activate

#### Download WebREPL
    cd ~/Projects/esp8266
    git clone https://github.com/micropython/webrepl.git

#### esp-open-sdk
    git clone --recursive https://github.com/pfalcon/esp-open-sdk.git
    cd esp-open-sdk
    unset LD_LIBRARY_PATH
    make

#### Building took ~20 minutes and outputs a command for adding path in the end
    export PATH=$HOME/Projects/esp8266/esp-open-sdk/xtensa-lx106-elf/bin:$PATH

#### Micropython & build formware
    cd ~/Projects/esp8266
    git clone --recursive https://github.com/micropython/micropython.git
    cd micropython/mpy-cross
    make
    cd ../esp8266
    make axtls
    make

#### Erase old and flash new firmware
    esptool.py image_info $HOME/Projects/esp8266/micropython/esp8266/build/firmware-combined.bin
    sudo esptool.py --port /dev/ttyUSB0 erase_flash
    sudo esptool.py --port /dev/ttyUSB0 --baud 230400 write_flash -fm dio -fs 32m -ff 40m 0x00000 $HOME/Projects/esp8266/micropython/esp8266/build/firmware-combined.bin

Now you can connect to the esp8266 with picocom to get Python REPL, or via Wifi and WebREPL: Esp8266 acts as a Wifi AP (SSID MicroPython-xxxxxx) and password is micropythoN
- IP address is 192.168.4.1
- boot.py - this file is for setting up the connection
- main.py - this file is the main application

#### Get boot.py from esp8266 via USB and save it with a different name locally
    sudo ampy --port /dev/ttyUSB0 get boot.py board_boot.py

#### Upload boot.py via Wifi and WebREPL
    cd ~/Projects/esp8266/webrepl
    python webrepl_cli.py boot.py 192.168.4.1:/boot.py
